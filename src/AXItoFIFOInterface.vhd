----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/31/2019 10:46:49 AM
-- Design Name: 
-- Module Name: AXItoFIFOInterface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AXItoFIFOInterface is
    Port ( clk : in STD_LOGIC;
           wr_en : out STD_LOGIC;
           new_cmd : in STD_LOGIC;
           data_in : out STD_LOGIC_VECTOR(15 downto 0);
           rst : in STD_LOGIC;
           command : in STD_LOGIC_VECTOR (31 downto 0);
           payload : in STD_LOGIC_VECTOR (15 downto 0));
end AXItoFIFOInterface;

architecture Behavioral of AXItoFIFOInterface is

 signal cmdwr_cnt : integer := 0 ;
 
 constant sync_word : std_logic_vector(15 downto 0) := b"1000000101111110";
 constant rwreg : std_logic_vector(15 downto 0) := b"0110011001100110";
 constant set_bit : std_logic_vector(15 downto 0) := b"0110010101100101";
 constant no_op : std_logic_vector(15 downto 0) := b"0110100101101001";

begin

	 --When new data detected, check which type of command and write to FIFO the header, the ADDRESS/ID and the payload
    process( clk ) is
    begin
      if (rising_edge (clk)) then
        if(rst = '1') then 
            wr_en <= '0';
            data_in <= (others => '0');
            cmdwr_cnt <= 0;
                        
        elsif(new_cmd = '1') then
	       if(command(3 downto 0) = "0001") then 	       	         
	             case cmdwr_cnt is
                    when 0 =>
	                   data_in <= rwreg;
	                   wr_en <= '1';
	                when 1 =>
	                   data_in <= "1010011001101010";
	                   wr_en <= '1';
	                when 2 =>
	                   data_in <= payload;
	                   wr_en <= '1';
                    when others =>
                       wr_en <= '0';
                   end case;
                   
                   if (cmdwr_cnt = 3) then
                        cmdwr_cnt <= 0;
                   else 
                        cmdwr_cnt <= cmdwr_cnt+1;
                   end if;
                   
	       elsif (command(3 downto 0) = "0010") then 
                 case cmdwr_cnt is
                    when 0 =>
                       data_in <= set_bit;
                       wr_en <= '1';
                    when 1 =>
                       data_in <= "1010011001101010";
                       wr_en <= '1';
                    when 2 =>
                       data_in <= payload;
                       wr_en <= '1';
                    when others =>
                       wr_en <= '0';
                   end case;
                   
                   if (cmdwr_cnt = 3) then
                        cmdwr_cnt <= 0;
                   else 
                        cmdwr_cnt <= cmdwr_cnt+1;
                   end if;   
	       else 
	               cmdwr_cnt <= 0;	     
	       end if;
	    end if;
	  end if;
    end process;

end Behavioral;
