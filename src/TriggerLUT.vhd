----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/30/2019 12:38:25 PM
-- Design Name: 
-- Module Name: TriggerLUT - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TriggerLUT is
    port (
        input : in std_logic_vector(3 downto 0);
        trigger_cmd  : out std_logic_vector(7 downto 0)
    );
end entity;

architecture any of TriggerLUT is
begin

    process (input) is
    begin
        case input is      
        --abcd|xyz
        when "0001" => trigger_cmd <= "00101011";
        when "0010" => trigger_cmd <= "00101101";
        when "0011" => trigger_cmd <= "00101110";
        when "0100" => trigger_cmd <= "00110011";
        when "0101" => trigger_cmd <= "00110101";
        when "0110" => trigger_cmd <= "00110110";
        when "0111" => trigger_cmd <= "00111001";
        when "1000" => trigger_cmd <= "00111010";
        when "1001" => trigger_cmd <= "00111100";
        when "1010" => trigger_cmd <= "01001011";
        when "1011" => trigger_cmd <= "01001101";
        when "1100" => trigger_cmd <= "01001110";
        when "1101" => trigger_cmd <= "01010011";
        when "1110" => trigger_cmd <= "01010101";
        when "1111" => trigger_cmd <= "01010110";
        when "0000" => trigger_cmd <= "00000000";
        when others => trigger_cmd <= "00000000";
        end case;
    end process;
end architecture; 
