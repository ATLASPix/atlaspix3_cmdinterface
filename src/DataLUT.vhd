----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/30/2019 12:38:25 PM
-- Design Name: 
-- Module Name: TriggerLUT - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DataLUT is
    port (
        input_data : in std_logic_vector(4 downto 0);
        encoded_data  : out std_logic_vector(7 downto 0)
    );
end entity;

architecture any of DataLUT is
begin

    process (input_data) is
    begin
        case input_data is      
        when "00000" => encoded_data <= "01101010";
        when "00001" => encoded_data <= "01101100";
        when "00010" => encoded_data <= "01110001";
        when "00011" => encoded_data <= "01110010";
        when "00100" => encoded_data <= "01110100";
        when "00101" => encoded_data <= "10001011";
        when "00110" => encoded_data <= "10001101";
        when "00111" => encoded_data <= "10001110";
        when "01000" => encoded_data <= "10010011";
        when "01001" => encoded_data <= "10010101";
        when "01010" => encoded_data <= "10010110";
        when "01011" => encoded_data <= "10011001";
        when "01100" => encoded_data <= "10011010";
        when "01101" => encoded_data <= "10011100";
        when "01110" => encoded_data <= "10100011";
        when "01111" => encoded_data <= "10100101";
        when "10000" => encoded_data <= "10100110";
        when "10001" => encoded_data <= "10101001";
        when "10010" => encoded_data <= "10101010";
        when "10011" => encoded_data <= "10101100";
        when "10100" => encoded_data <= "10110001";
        when "10101" => encoded_data <= "10110010";
        when "10110" => encoded_data <= "10110100";
        when "10111" => encoded_data <= "11000011";
        when "11000" => encoded_data <= "11000101";
        when "11001" => encoded_data <= "11000110";
        when "11010" => encoded_data <= "11001001";
        when "11011" => encoded_data <= "11001010";
        when "11100" => encoded_data <= "11001100";
        when "11101" => encoded_data <= "11010001";
        when "11110" => encoded_data <= "11010010";
        when "11111" => encoded_data <= "11010100"; 
        when others  => encoded_data <= "00000000";              
        end case;
    end process;
end architecture; 
