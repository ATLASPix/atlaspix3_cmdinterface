----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/31/2019 09:11:04 AM
-- Design Name: 
-- Module Name: CMDInterfaceCore - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CMDInterfaceCore is
    Port ( reference_clk : in STD_LOGIC;
           clk40 : in std_logic;
           rst : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (15 downto 0);
           empty : in STD_LOGIC;
           rd_en : out STD_LOGIC;
           atp3_cmd : out STD_LOGIC;
           write_cpu : in std_logic;
           trigger_in : in std_logic;
           delay : in unsigned(31 downto 0);
           debug : out std_logic_vector(15 downto 0)
           );
end CMDInterfaceCore;

architecture Behavioral of CMDInterfaceCore is

constant sync_word : std_logic_vector(15 downto 0) := b"1000000101111110";
constant rwreg : std_logic_vector(15 downto 0) := b"0110011001100110";
constant set_bit : std_logic_vector(15 downto 0) := b"0110010101100101";
constant no_op : std_logic_vector(15 downto 0) := b"0110100101101001";


constant max_cmd : integer := 3;

signal SR_cnt : integer :=0;
signal SR_current_word : std_logic_vector(15 downto 0) := sync_word;
signal cmd_cnt : integer :=0;
signal trigger_cmdwrdcnt_value : unsigned(63 downto 0) := x"0000FEDCBAFEFEDC";
signal trigger_cmdwrdcnt_value_vector : std_logic_vector(63 downto 0);
signal trigger_patternreg : std_logic_vector(7 downto 0);
signal trigger_cnt : unsigned(4 downto 0):=(others => '0');
signal trigger_cnt_enc : std_logic_vector(7 downto 0):=(others => '0');
signal prev_new_trigger : std_logic;


signal trigger_fifo_full, trigger_fifo_empty, trigger_fifo_wr_en,trigger_fifo_rd_en : std_logic;

--trigger related
--signal clk40 : std_logic;
signal trigger_rising_edge : std_logic;
signal BC_cntvec : std_logic_vector(47 downto 0):= (others => '0');
signal BC_cnt : unsigned(47 downto 0):= (others => '0');

signal trigger_arrival_BC : unsigned(47 downto 0):= (others => '0');
signal trigger_emmit_BC  : unsigned(47 downto 0):= (others => '0');
signal cmdwrdcnt  : unsigned(47 downto 0):= (others => '0');
signal CMDWRD_arrival  : unsigned(47 downto 0):= (others => '0');
signal CMDWRD_emmit: unsigned(47 downto 0):= (others => '0');
signal trigger_offset : unsigned(2 downto 0) := (others => '0');
signal trigger_pattern : std_logic_vector(3 downto 0);
signal trigger_cmd : std_logic_vector(7 downto 0);
signal new_trigger : std_logic;
signal trigger_fifo_input : std_logic_vector(63 downto 0);
signal trigger_processed : std_logic := '0';
signal new_trigger_data : std_logic :='0';

signal trigger_processed_cnt : integer := 0;
signal trigger_processed_clk40 : std_logic;
signal trigger_fifo_rd_en_buf : std_logic :='0';

COMPONENT DataLUT is
              PORT (
                  input_data : in std_logic_vector(4 downto 0);
                  encoded_data  : out std_logic_vector(7 downto 0)
              );
          END COMPONENT;
          
COMPONENT trigger_fifo
            PORT (
              clk : IN STD_LOGIC;
              srst : IN STD_LOGIC;
              din : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
              wr_en : IN STD_LOGIC;
              rd_en : IN STD_LOGIC;
              dout : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
              full : OUT STD_LOGIC;
              empty : OUT STD_LOGIC
            );
          END COMPONENT;

--COMPONENT clkdiv
--         generic (DIVRATIO : integer := 4);
--         port (
--             clk     : in std_logic;
--             reset  : in std_logic;
--             clkout  : out std_logic
--         );
--     END COMPONENT;    

COMPONENT edge_detection 
         Port ( clk : in STD_LOGIC;
                I : in STD_LOGIC;
                redge : out STD_LOGIC;
                fedge : out STD_LOGIC;
                transistion : out STD_LOGIC);
     end COMPONENT; 

COMPONENT TriggerLUT is
         port (
             input : in std_logic_vector(3 downto 0);
             trigger_cmd  : out std_logic_vector(7 downto 0)
         );
     END COMPONENT;

COMPONENT c_counter_binary_0
  PORT (
    CLK : IN STD_LOGIC;
    SCLR : IN STD_LOGIC;
    Q : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
  );
END COMPONENT;

begin

       trigger_fifo_inst : trigger_fifo
            PORT MAP (
                clk => reference_clk,
                srst => rst,
                din => trigger_fifo_input,
                wr_en => trigger_fifo_wr_en,
                rd_en => trigger_fifo_rd_en,
                dout => trigger_cmdwrdcnt_value_vector,
                full => trigger_fifo_full,
                empty => trigger_fifo_empty
  );
       
    trigger_cmdwrdcnt_value <= unsigned(trigger_cmdwrdcnt_value_vector);   
       
-- -- Generate 40MHz clock from 160MHz reference 
--    uut: clkdiv generic map ( DIVRATIO =>  4)
--       port map ( clk      => reference_clk,
--                  reset   => rst,
--                  clkout   => clk40 );
  
  
  -- edge detection of trigger input at 40MHz
    trigger_edge_detect : edge_detection
       port map ( clk => clk40,
                   I => trigger_in,
                   redge => trigger_rising_edge
        );
  
  -- 40MHz BC counter
  

  
BCCNT_BCinst : c_counter_binary_0
  PORT MAP (
    CLK => clk40,
    SCLR => rst,
    Q => BC_cntvec
  );
  
  BC_cnt <=unsigned(BC_cntvec);
  --cmdwrdcnt(45 downto 0) <= unsigned(BC_cntvec(47 downto 2));
  
  -- when trigger arise, save its BC number, compute the correct cmd word to emit the trigger and compute the offset to determine the trigger pattern
  process( clk40 ) is
  begin
    if (rst = '1') then 
    trigger_arrival_BC <= (others => '0');
    CMDWRD_arrival <= (others => '0');
    trigger_emmit_BC <= (others => '0');
    CMDWRD_emmit <= (others => '0');
    trigger_offset <= (others => '1');
    
    elsif (rising_edge (clk40)) then
    
    -- BC number in 40MHz units
    trigger_arrival_BC <= BC_cnt;
    -- CMDWRD index in 100 ns units (16bit at 160MHz)
    CMDWRD_arrival <= cmdwrdcnt;
    -- when to emmit the trigger to interface in 40MHz units
    trigger_emmit_BC <= BC_cnt + delay;
    --when to emmit the trigger in cmdwrds index
    CMDWRD_emmit <= cmdwrdcnt + (delay srl 2) + (((BC_cnt(2 downto 0))+(delay(2 downto 0))) srl 2);
    -- which sub cmdwrd bi to emmit trigger at, should be 0 to 3 
    
    trigger_offset <= ((BC_cnt(2 downto 0))+(delay(2 downto 0)));
    
    trigger_fifo_input(47 downto 0) <= std_logic_vector(CMDWRD_emmit);
    trigger_fifo_input(63 downto 56) <= std_logic_vector(trigger_cmd);
    trigger_fifo_input(55 downto 48) <= std_logic_vector(trigger_cnt_enc);

      if(trigger_rising_edge = '1') then 
              new_trigger_data <= '1';
              trigger_cnt <= trigger_cnt +1;

      else 
              if(trigger_processed_clk40 ='1' and trigger_fifo_empty = '0') then
                trigger_fifo_rd_en_buf <= '1';
              else
                 trigger_fifo_rd_en_buf <= '0';             
              end if;
              
              if (new_trigger_data ='1') then 
                   trigger_fifo_wr_en <= '1'; 
                   new_trigger_data <='0';
              else
                trigger_fifo_wr_en <= '0';
              end if;
      end if;
    end if;
  end process;      

  -- edge detection of trigger input at 40MHz
    wr_en_edge_detect : edge_detection
       port map ( clk => clk40,
                   I => trigger_fifo_rd_en_buf,
                   redge => trigger_fifo_rd_en
        );


  --encode trigger_pattern thermometer like
  process (trigger_offset) is
  begin
      case trigger_offset is      
      --abcd|xyz
      when "000" => trigger_pattern <= "1000";
      when "001" => trigger_pattern <= "0100";
      when "010" => trigger_pattern <= "0010";
      when "011" => trigger_pattern <= "0001";
      when "100" => trigger_pattern <= "1000";
      when "101" => trigger_pattern <= "0100";
      when "110" => trigger_pattern <= "0010";
      when "111" => trigger_pattern <= "0001";     
      when others => trigger_pattern <= "0000";
      end case;
  end process;
  -- encode the trigger command following the pattern   
  
  trigger_lut_inst : TriggerLUT 
      port map (
          input => trigger_pattern,
          trigger_cmd => trigger_cmd
      );
       
       
      
       
      process( reference_clk ) is
           begin
             if (rising_edge(reference_clk)) then
                if(rst = '1') then
                    atp3_cmd <='0';
                    SR_cnt <= 0;
                    cmdwrdcnt <= (others => '0');                    
                else  
                    atp3_cmd <= SR_current_word(SR_cnt);
                    if SR_cnt < 15 then
                        SR_cnt <= SR_cnt +1;
                    else 
                        SR_cnt <= 0;
                        cmdwrdcnt <= cmdwrdcnt + 1;
                    end if;
                 end if;    
             end if;
       end process;
        
       
       -- encode the trigger count in the trigger command tag
       DATAENCODERtrTag : DataLUT
             PORT MAP(
                 input_data => std_logic_vector(trigger_cnt),
                 encoded_data => trigger_cnt_enc
             );
       

       debug <= SR_current_word;
       process( reference_clk ) is
           begin
             if (rising_edge(reference_clk)) then          
                trigger_processed <= '0';
                if (rst='1') then 
                    rd_en <='0';
                    SR_current_word <= (others => '0');
                    cmd_cnt <= 0;
                    trigger_processed <= '0';

                elsif ((SR_cnt = 15) and (cmdwrdcnt = (trigger_cmdwrdcnt_value(47 downto 0) - 1)) and write_cpu='0') then 
                    SR_current_word(15 downto 0) <= std_logic_vector(trigger_cmdwrdcnt_value(63 downto 48));
                    rd_en <= '0';
                    trigger_processed <= '1';
                    
                elsif (SR_cnt = 15 and empty = '1') then
                    SR_current_word <= sync_word;
                    rd_en <= '0';
                    trigger_processed <= '0';
                
                elsif (SR_cnt = 15 and empty='0' and write_cpu = '1') then 
                    trigger_processed <= '0';
                    if(cmd_cnt< max_cmd ) then
                        rd_en <= '1';
                        SR_current_word <= din;
                        cmd_cnt <= cmd_cnt+1;
                    else
                        SR_current_word <= sync_word;
                        rd_en <= '0';
                        cmd_cnt <= 0;
                    end if;
                elsif (SR_cnt = 4 and trigger_processed ='1') then 
                        trigger_processed <= '0';
                else
                    rd_en <= '0';
                end if;    
             end if;
       end process;

       process( reference_clk ) is
           begin
             if (rising_edge(reference_clk)) then 
                if trigger_processed = '1' then 
                    trigger_processed_clk40 <='1';
                    trigger_processed_cnt <= 0;
                elsif(trigger_processed_clk40 <= '1' and trigger_processed_cnt = 4 ) then 
                    trigger_processed_clk40 <='0';
                    trigger_processed_cnt <= 0;                  
                else
                    trigger_processed_cnt <= trigger_processed_cnt +1;
                end if;
              end if;
          end process;

end Behavioral;
