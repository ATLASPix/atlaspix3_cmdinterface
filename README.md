#ATLASPix3 Command Interface (based on RD53 cmd interface protocol)


##AXI registers: 

###Configuration registers

- **register 0** (0x0) COMMAND, 32 bits : command to be sent
	- value 0x1 : RW command
	- value 0x2 : SET_BIT command

- **register 1** (0x4) PAYLOAD, 10 bits : payload for RW or SET_BIT commands, before encoding (done in FPGA)

- **register 2** (0x8) WRITE, 1bit : Write commands in fifo to CMD link 

When a new command is written through AXI bus to the COMMAND register, the PAYLOAD register is read and the corresponding command and data payload are sent to a FIFO

When all the commands you want to issue are stored in the internal FIFO, write 0x1 to the WRITE register to unload the command in the CMD interface line  

###RESET register

- **register 3** (0xC), 2 bits : reset command for internal blocks
	- bit 0 : reset AXI to FIFO block (active high)
	- bit 1 : reset CMD interface core (active high)


###Trigger register

- **register 4** (0x10), 32 bits : Delay in 25ns units between trigger reception and emmision to ATLASPix3



## Description of the IP 

This interface provide the mean of configuration and triggering of the ATLASPix3 through the CMD interface. 100 ns words (16 bits ),  at 160 MHz are sent to the interface pins of ATLASPix3. The interface outputs the synchronisation word by default when no other command are issued. 

If the configuration FIFO is filled with RW or SET_BIT command, asserting the WRITE command will shift the commands stored in the FIFO to the CMD line. A synchronisation word is output between each command block. 

If a trigger is detector from the trigger input, the trigger is timestamped with 40 MHz and 160 MHz timestamps and the trigger is issued after the programmed delay through the command interface. Single 25 ns bin trigger supported for the moment.


